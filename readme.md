# Overview #

The BeagleBone Black is capable of booting of its internal memory, a microsd card, or the network. This repository has been set up to help create a bootable microsd card with two partitions. The BOOT partition that hosts a bootloader and Linux 3.18.9-rt kernel image, as well as a device tree that pinmuxes the board to support peripherals unique to our project. The ROOTFS parition contains a minimal debian filesystem, with a kernel module that sets up three parallel hardware time captures on TIMER4, TIMER5 and TIMER6. This allows us to time capture three parallel oscillators. In future, we will add support for radio time stamping and time syncronisation.

1. The roseline kernel module is based off pps-gmtimer: https://github.com/ddrown/pps-gmtimer
1. The radio time synchronsiation application uses lowpan-tools: https://github.com/linux-wpan/lowpan-tools
1. Syscalls were implemented using http://linuxdevicedriversviven.blogspot.com/2013/08/how-system-call-is-works-in-armx86-and.html
1. MMAP example was adapted from https://groups.google.com/forum/m/#!topic/beagleboard/lAFDOFPDwxs
1. A list of timer registers for the am3359 is given here: http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/sysbios/6_33_01_25/exports/bios_6_33_01_25/docs/cdoc/ti/sysbios/timers/dmtimer/doc-files/TimerTables.html

# Build Instructions #

1. Install Ubuntu 14.04 64-bit in a VM
1. Install git and checkout ROSELINE project
1. Prepare a 4GB+ MicroSD card
	1. Plug in the SD card into a reader on your host pc and it will mount as /dev/sdX. It is your responsibility to determine the correct X. Hint: ```tail -f /var/log/syslog```
	1. Start fdisk to partition the SD card: fdisk /dev/sdX
	1. Type o. This will clear out any partitions on the drive.
	1. Type p to list partitions. There should be no partitions left.
	1. Now type n, then p for primary, 1 for the first partition on the drive, enter to accept the default first sector, then +64M to set the size to 64MB.
	1. Type t to change the partition type, then e to set it to W95 FAT16 (LBA).
	1. Type a, then 1 to set the bootable flag on the first partition.
	1. Now type n, then p for primary, 2 for the second partition on the drive, and enter twice to select the default first and last sectors.
	1. Write the partition table and exit by typing w.
	1. Create the FAT16 filesystem:mkfs.vfat -F 16 /dev/sdX1
	1. Create the ext4 filesystem:mkfs.ext4 /dev/sdX2
1. Copy over the boot and root file system 
	mkdir -p tmp/boot
	sudo mount /dev/sdX1 tmp/boot
	sudo cp bootloader/* tmp/boot
	sudo umount tmp/boot
	mkdir -p tmp/rootfs
	sudo sudo mount /dev/sdX2 tmp
	sudo tar -xpjf rootfs/rootfs.tar.bz2 -C tmp/rootfs
	sudo umount tmp/boot

# Usage Instructions #

IMPORTANT -- The TCLKIN pin (P9.41) conflicts with HDMI. You will therefore not be able to plug a monitor into the BeagleBone Black. You will either need to use a FTDI cable for a serial console (J1) or login via SSH over ethernet. I prefer the latter, as it doesn't require extra hardware you can manage multiple boards this way.

1. Plug an ethernet cable into the BeagleBone Black (BBB).
1. Plug the microsd you prepared above into the BBB and apply power.
1. The BBB will request an IP over DHCP -- check your DHCP server for the address it was assigned.
1. SSH into the BeagleBone Black image with username 'root' and password 'roseline'.
1. The roseline.ko module will automatically create the following timers on boot:
	1. timer4 - driven by external oscillator (ext_) at 16MHz
	1. timer5 - driven by on-board system clock (int_) at 24MHz
	1. timer6 - driven by on-board RTC clock (rtc_) at 32.768kHz
1. You can perform a falling-edge (requirement for radio time stamping) hardware capture on each clock by pulling the following pins high:
	1. timer4 - BBB P8.7
	1. timer5 - BBB P8.9
	1. timer6 - BBB P8.10
1. To read the counter value at interrupt, print the contents of these files on the BBB
	1. timer4 - /sys/devices/roseline/ext_capture
	1. timer5 - /sys/devices/roseline/int_capture
	1. timer6 - /sys/devices/roseline/rtc_capture
1. An external oscillator (square wave) can be connected to P9.41. By default, the kernel assumes it's 16MHz. For different external oscillator frequencies, you will need to modify the following two patches and recompile the kernel + device tree:
    1. ./patches/arch_arm_mach-omap2_cclock3xxx_data.c.patch : 16000000 -> ?
	1. ./patches/arch_arm_boot_dts_am33xx-clocks.dtsi.patch : 16000000 -> ?

# Kernel compilation Instructions #

Coming soon

# Radio-based time synchronisation #

See app/rosline.cpp

# Memory-mapped timers #

See app/mmap.cpp