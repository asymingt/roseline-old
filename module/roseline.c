// Basic kernel module includes
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/string.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/pinctrl/consumer.h>
#include <linux/pps_kernel.h>
#include <linux/clocksource.h>

// OMAP-specific dual mode timer code
#include "../linux/arch/arm/plat-omap/include/plat/dmtimer.h"

// Module information
#define MODULE_NAME "roseline"
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Andrew Symington");
MODULE_DESCRIPTION("Linux 3.18.9-rt4 kernel driver for time synchronization");
MODULE_VERSION("0.1.0");

// Data specific to a clock
struct roseline_clock_data
{
  struct omap_dm_timer 		*capture_timer;
  const char 							*timer_name;
  uint32_t 								frequency;
  unsigned int 						capture;
  unsigned int 						overflow;
  unsigned int 						count_at_interrupt;
  struct pps_event_time 	ts;
  struct timespec 				delta;
  struct pps_device				*pps;
  struct pps_source_info 	info;
  struct clocksource 			clksrc;
  int                     ready;
};

// Collection of three clock sources
struct roseline_platform_data
{
  struct roseline_clock_data clk_rtc;	// RTC 32 kHz
  struct roseline_clock_data clk_int;	// EXT 10 MHz
  struct roseline_clock_data clk_ext;	// INT 24 MHz
};

/////////////////////////////////////////////////////////////////////////////////////////////
// FOR PRINTING OUT DEVICE ATTRIBUTES IN THE SYSFS TREE /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

static ssize_t ext_timer_name_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%s\n", pdata->clk_ext.timer_name);
}

static ssize_t ext_stats_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "capture: %u\noverflow: %u\n", pdata->clk_ext.capture, pdata->clk_ext.overflow);
}

static ssize_t ext_interrupt_delta_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%lld.%09ld\n", (long long)pdata->clk_ext.delta.tv_sec, pdata->clk_ext.delta.tv_nsec);
}

static ssize_t ext_pps_ts_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%lld.%09ld\n", (long long)pdata->clk_ext.ts.ts_real.tv_sec, pdata->clk_ext.ts.ts_real.tv_nsec);
}

static ssize_t ext_count_at_interrupt_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%u\n", pdata->clk_ext.count_at_interrupt);
}

static ssize_t ext_capture_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%u\n",
	__omap_dm_timer_read(pdata->clk_ext.capture_timer, OMAP_TIMER_CAPTURE_REG, pdata->clk_ext.capture_timer->posted)
	);
}

static ssize_t ext_ctrlstatus_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct roseline_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%x\n",
	__omap_dm_timer_read(pdata->clk_ext.capture_timer, OMAP_TIMER_CTRL_REG, pdata->clk_ext.capture_timer->posted)
	);
}

static ssize_t ext_timer_counter_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  unsigned int current_count = 0;
  current_count = omap_dm_timer_read_counter(pdata->clk_ext.capture_timer);
  return sprintf(buf, "%u\n", current_count);
}

static DEVICE_ATTR(ext_timer_name, 					S_IRUGO, ext_timer_name_show, 				NULL);
static DEVICE_ATTR(ext_stats, 							S_IRUGO, ext_stats_show, 							NULL);
static DEVICE_ATTR(ext_interrupt_delta, 		S_IRUGO, ext_interrupt_delta_show, 		NULL);
static DEVICE_ATTR(ext_pps_ts, 							S_IRUGO, ext_pps_ts_show, 						NULL);
static DEVICE_ATTR(ext_count_at_interrupt, 	S_IRUGO, ext_count_at_interrupt_show, NULL);
static DEVICE_ATTR(ext_capture, 						S_IRUGO, ext_capture_show, 						NULL);
static DEVICE_ATTR(ext_ctrlstatus, 					S_IRUGO, ext_ctrlstatus_show, 				NULL);
static DEVICE_ATTR(ext_timer_counter, 			S_IRUGO, ext_timer_counter_show, 			NULL);

// FOR INTERNAL CLOCK

static ssize_t int_timer_name_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%s\n", pdata->clk_int.timer_name);
}

static ssize_t int_stats_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "capture: %u\noverflow: %u\n", pdata->clk_int.capture, pdata->clk_int.overflow);
}

static ssize_t int_interrupt_delta_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%lld.%09ld\n", (long long)pdata->clk_int.delta.tv_sec, pdata->clk_int.delta.tv_nsec);
}

static ssize_t int_pps_ts_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%lld.%09ld\n", (long long)pdata->clk_int.ts.ts_real.tv_sec, pdata->clk_int.ts.ts_real.tv_nsec);
}

static ssize_t int_count_at_interrupt_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%u\n", pdata->clk_int.count_at_interrupt);
}

static ssize_t int_capture_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%u\n",
	__omap_dm_timer_read(pdata->clk_int.capture_timer, OMAP_TIMER_CAPTURE_REG, pdata->clk_int.capture_timer->posted)
	);
}

static ssize_t int_ctrlstatus_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct roseline_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%x\n",
	__omap_dm_timer_read(pdata->clk_int.capture_timer, OMAP_TIMER_CTRL_REG, pdata->clk_int.capture_timer->posted)
	);
}

static ssize_t int_timer_counter_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  unsigned int current_count = 0;
  current_count = omap_dm_timer_read_counter(pdata->clk_int.capture_timer);
  return sprintf(buf, "%u\n", current_count);
}

static DEVICE_ATTR(int_timer_name, 					S_IRUGO, int_timer_name_show, 				NULL);
static DEVICE_ATTR(int_stats, 							S_IRUGO, int_stats_show, 							NULL);
static DEVICE_ATTR(int_interrupt_delta, 		S_IRUGO, int_interrupt_delta_show, 		NULL);
static DEVICE_ATTR(int_pps_ts, 							S_IRUGO, int_pps_ts_show, 						NULL);
static DEVICE_ATTR(int_count_at_interrupt, 	S_IRUGO, int_count_at_interrupt_show, NULL);
static DEVICE_ATTR(int_capture, 						S_IRUGO, int_capture_show, 						NULL);
static DEVICE_ATTR(int_ctrlstatus, 					S_IRUGO, int_ctrlstatus_show, 				NULL);
static DEVICE_ATTR(int_timer_counter, 			S_IRUGO, int_timer_counter_show, 			NULL);

// FOR REALTIME CLOCK

static ssize_t rtc_timer_name_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%s\n", pdata->clk_rtc.timer_name);
}

static ssize_t rtc_stats_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "capture: %u\noverflow: %u\n", pdata->clk_rtc.capture, pdata->clk_rtc.overflow);
}

static ssize_t rtc_interrupt_delta_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%lld.%09ld\n", (long long)pdata->clk_rtc.delta.tv_sec, pdata->clk_rtc.delta.tv_nsec);
}

static ssize_t rtc_pps_ts_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%lld.%09ld\n", (long long)pdata->clk_rtc.ts.ts_real.tv_sec, pdata->clk_rtc.ts.ts_real.tv_nsec);
}

static ssize_t rtc_count_at_interrupt_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%u\n", pdata->clk_rtc.count_at_interrupt);
}

static ssize_t rtc_capture_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  return sprintf(buf, "%u\n",
	__omap_dm_timer_read(pdata->clk_rtc.capture_timer, OMAP_TIMER_CAPTURE_REG, pdata->clk_rtc.capture_timer->posted)
	);
}

static ssize_t rtc_ctrlstatus_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct roseline_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%x\n",
	__omap_dm_timer_read(pdata->clk_rtc.capture_timer, OMAP_TIMER_CTRL_REG, pdata->clk_rtc.capture_timer->posted)
	);
}

static ssize_t rtc_timer_counter_show(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct roseline_platform_data *pdata = dev->platform_data;
  unsigned int current_count = 0;
  current_count = omap_dm_timer_read_counter(pdata->clk_rtc.capture_timer);
  return sprintf(buf, "%u\n", current_count);
}

static DEVICE_ATTR(rtc_timer_name, 					S_IRUGO, rtc_timer_name_show, 				NULL);
static DEVICE_ATTR(rtc_stats, 							S_IRUGO, rtc_stats_show, 							NULL);
static DEVICE_ATTR(rtc_interrupt_delta, 		S_IRUGO, rtc_interrupt_delta_show, 		NULL);
static DEVICE_ATTR(rtc_pps_ts, 							S_IRUGO, rtc_pps_ts_show, 						NULL);
static DEVICE_ATTR(rtc_count_at_interrupt, 	S_IRUGO, rtc_count_at_interrupt_show, NULL);
static DEVICE_ATTR(rtc_capture, 						S_IRUGO, rtc_capture_show, 						NULL);
static DEVICE_ATTR(rtc_ctrlstatus, 					S_IRUGO, rtc_ctrlstatus_show, 				NULL);
static DEVICE_ATTR(rtc_timer_counter, 			S_IRUGO, rtc_timer_counter_show, 			NULL);

// COMPLETE / EXHAUSTIVE ATTRIBUTE LIST

static struct attribute *attrs[] = {
  &dev_attr_ext_timer_counter.attr,
  &dev_attr_ext_ctrlstatus.attr,
  &dev_attr_ext_capture.attr,
  &dev_attr_ext_count_at_interrupt.attr,
  &dev_attr_ext_pps_ts.attr,
  &dev_attr_ext_interrupt_delta.attr,
  &dev_attr_ext_stats.attr,
  &dev_attr_ext_timer_name.attr,
  &dev_attr_int_timer_counter.attr,
  &dev_attr_int_ctrlstatus.attr,
  &dev_attr_int_capture.attr,
  &dev_attr_int_count_at_interrupt.attr,
  &dev_attr_int_pps_ts.attr,
  &dev_attr_int_interrupt_delta.attr,
  &dev_attr_int_stats.attr,
  &dev_attr_int_timer_name.attr,
  &dev_attr_rtc_timer_counter.attr,
  &dev_attr_rtc_ctrlstatus.attr,
  &dev_attr_rtc_capture.attr,
  &dev_attr_rtc_count_at_interrupt.attr,
  &dev_attr_rtc_pps_ts.attr,
  &dev_attr_rtc_interrupt_delta.attr,
  &dev_attr_rtc_stats.attr,
  &dev_attr_rtc_timer_name.attr,
  NULL,
};

static struct attribute_group attr_group = {
  .attrs = attrs,
};

/////////////////////////////////////////////////////////////////////////////////////////////
// GENERIC INTERRUPT HANDLER  ///////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

static irqreturn_t roseline_interrupt(int irq, void *data)
{
  struct roseline_clock_data *pdata;

  pdata = data;
  if (pdata->ready)
  {
    unsigned int irq_status;

    // If the OMAP_TIMER_INT_CAPTURE flag in the IRQ status is set
    irq_status = omap_dm_timer_read_status(pdata->capture_timer);
    if (irq_status & OMAP_TIMER_INT_CAPTURE)
    {
      uint32_t ps_per_hz;
      unsigned int count_at_capture;
      pdata->count_at_interrupt = omap_dm_timer_read_counter(pdata->capture_timer);
      count_at_capture = __omap_dm_timer_read(pdata->capture_timer, OMAP_TIMER_CAPTURE_REG, pdata->capture_timer->posted);
      pdata->delta.tv_sec = 0;

      // use picoseconds per hz to avoid floating point and limit the rounding error
      ps_per_hz = 1000000000 / (pdata->frequency / 1000);
      pdata->delta.tv_nsec = ((pdata->count_at_interrupt - count_at_capture) * ps_per_hz) / 1000;
      pdata->capture++;

      __omap_dm_timer_write_status(pdata->capture_timer, OMAP_TIMER_INT_CAPTURE);
    }

    // If the overflow flag in the IRQ status is set
    if (irq_status & OMAP_TIMER_INT_OVERFLOW)
    {
      pdata->overflow++;
      __omap_dm_timer_write_status(pdata->capture_timer, OMAP_TIMER_INT_OVERFLOW);
    }
  }

  return IRQ_HANDLED;
}

// Set up the timer capture 
static void omap_dm_timer_setup_capture(struct omap_dm_timer *timer)
{
  u32 ctrl;

  // Set the timer source
  omap_dm_timer_set_source(timer, OMAP_TIMER_SRC_SYS_CLK);
  omap_dm_timer_enable(timer);

  // Read the control register
  ctrl = __omap_dm_timer_read(timer, OMAP_TIMER_CTRL_REG, timer->posted);

  // Disable prescaler
  ctrl &= ~(OMAP_TIMER_CTRL_PRE | (0x07 << 2));

  // Autoreload
  ctrl |= OMAP_TIMER_CTRL_AR;
  __omap_dm_timer_write(timer, OMAP_TIMER_LOAD_REG, 0, timer->posted);

  // Start timer
  ctrl |= OMAP_TIMER_CTRL_ST;

  // Set capture
  ctrl |= OMAP_TIMER_CTRL_TCM_HIGHTOLOW | OMAP_TIMER_CTRL_GPOCFG; // TODO: configurable direction
  __omap_dm_timer_load_start(timer, ctrl, 0, timer->posted);

  // Save the context
  timer->context.tclr = ctrl;
  timer->context.tldr = 0;
  timer->context.tcrr = 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// INIT, CONFIG AND KILL VARIOUS TIMERS /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

// Use a given clock : OMAP_TIMER_SRC_EXT_CLK, OMAP_TIMER_SRC_32_KHZ, OMAP_TIMER_SRC_SYS_CLK
static void omap_dm_timer_use_clk(struct roseline_clock_data *pdata, int clk)
{
  struct clk *gt_fclk;
  omap_dm_timer_set_source(pdata->capture_timer, clk);
  gt_fclk = omap_dm_timer_get_fclk(pdata->capture_timer);
  pdata->frequency = clk_get_rate(gt_fclk);
  switch (clk)
  {
  	case OMAP_TIMER_SRC_SYS_CLK: pr_info("timer(%s) switched to int, rate=%uHz\n", pdata->timer_name, pdata->frequency); break;
  	case OMAP_TIMER_SRC_32_KHZ:  pr_info("timer(%s) switched to rtc, rate=%uHz\n", pdata->timer_name, pdata->frequency); break;
  	case OMAP_TIMER_SRC_EXT_CLK: pr_info("timer(%s) switched to ext, rate=%uHz\n", pdata->timer_name, pdata->frequency); break;
  }  
}

// Enable the interrupt request
static void roseline_enable_irq(struct roseline_clock_data *pdata)
{
  unsigned int interrupt_mask;
  interrupt_mask = OMAP_TIMER_INT_CAPTURE | OMAP_TIMER_INT_OVERFLOW;
  __omap_dm_timer_int_enable(pdata->capture_timer, interrupt_mask);
  pdata->capture_timer->context.tier = interrupt_mask;
  pdata->capture_timer->context.twer = interrupt_mask;
}

// Initialise a timer
static int roseline_init_timer(struct device_node *timer_dn, struct roseline_clock_data *pdata)
{
  struct clk *gt_fclk;

  of_property_read_string_index(timer_dn, "ti,hwmods", 0, &pdata->timer_name);
  if (!pdata->timer_name)
  {
    pr_err("ti,hwmods property missing?\n");
    return -ENODEV;
  }

  pdata->capture_timer = omap_dm_timer_request_by_node(timer_dn);
  if (!pdata->capture_timer)
  {
    pr_err("request_by_node failed\n");
    return -ENODEV;
  }

  // TODO: use devm_request_irq?
  if (request_irq(pdata->capture_timer->irq, roseline_interrupt, IRQF_TIMER, MODULE_NAME, pdata))
  {
    pr_err("cannot register IRQ %d\n", pdata->capture_timer->irq);
    return -EIO;
  }

  // Print the timer information
  omap_dm_timer_setup_capture(pdata->capture_timer);
  gt_fclk = omap_dm_timer_get_fclk(pdata->capture_timer);
  pdata->frequency = clk_get_rate(gt_fclk);
  pr_info("timer name=%s rate=%uHz\n", pdata->timer_name, pdata->frequency);

  return 0;
}

// Cleanup a timer
static void roseline_cleanup_timer(struct roseline_clock_data *pdata)
{
  if (pdata->capture_timer)
  {
    omap_dm_timer_set_source(pdata->capture_timer, OMAP_TIMER_SRC_SYS_CLK); // in case TCLKIN is stopped during boot
    omap_dm_timer_set_int_disable(pdata->capture_timer, OMAP_TIMER_INT_CAPTURE | OMAP_TIMER_INT_OVERFLOW);
    free_irq(pdata->capture_timer->irq, pdata);
    omap_dm_timer_stop(pdata->capture_timer);
    omap_dm_timer_free(pdata->capture_timer);
    pdata->capture_timer = NULL;
    pr_info("Exiting.\n");
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// INIT, CONFIG AND KILL VARIOUS TIMERS /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

static struct roseline_platform_data *of_get_roseline_pdata(struct platform_device *pdev)
{
  struct device_node *np = pdev->dev.of_node, *timer_dn_ext, *timer_dn_int, *timer_dn_rtc;
  struct roseline_platform_data *pdata;
  const __be32 *timer_phandle_ext, *timer_phandle_rtc, *timer_phandle_int;
  int err = 0;

  // Get the 
  pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
  if (!pdata)
    return NULL;
  pdata->clk_int.ready = 0;
  pdata->clk_ext.ready = 0;
  pdata->clk_rtc.ready = 0;

  // *********************** 24Mhz TC ************************************
  
  timer_phandle_int = of_get_property(np, "timer_int", NULL);
  if (!timer_phandle_int)
  {
    pr_err("timer_int property in devicetree null\n");
    err = 1;
  }
  timer_dn_int = of_find_node_by_phandle(be32_to_cpup(timer_phandle_int));
  if (!timer_dn_int)
  {
    pr_err("timer_phandle_int find_node_by_phandle failed\n");
    err = 1;
  }
  if (roseline_init_timer(timer_dn_int, &(pdata->clk_int)) < 0)
  	err = 1;
  of_node_put(timer_dn_int);

  // *********************** 32Khz TC ************************************
  
  timer_phandle_rtc = of_get_property(np, "timer_rtc", NULL);
  if (!timer_phandle_rtc)
  {
    pr_err("timer_rtc property in devicetree null\n");
    err = 1;
  }
  timer_dn_rtc = of_find_node_by_phandle(be32_to_cpup(timer_phandle_rtc));
  if (!timer_dn_rtc)
  {
    pr_err("timer_phandle_rtc find_node_by_phandle failed\n");
    err = 1;
  }
  if (roseline_init_timer(timer_dn_rtc, &(pdata->clk_rtc)) < 0) 
	err = 1;
  of_node_put(timer_dn_rtc);
  
  // *********************** 10MHz TC ************************************
  
  timer_phandle_ext = of_get_property(np, "timer_ext", NULL);
  if (!timer_phandle_ext)
  {
    pr_err("timer_ext property in devicetree null\n");
    err = 1;
  }
  timer_dn_ext = of_find_node_by_phandle(be32_to_cpup(timer_phandle_ext));
  if (!timer_dn_ext)
  {
    pr_err("timer_phandle_ext find_node_by_phandle failed\n");
    err = 1;
  }
  if (roseline_init_timer(timer_dn_ext, &(pdata->clk_ext)) < 0)
	err = 1;
  of_node_put(timer_dn_ext);

  // If there's an error, clean up correctly
  if (err)
  {
  	devm_kfree(&pdev->dev, pdata);	
  	return NULL;
  }

  // Return data
  return pdata;
}

static const struct of_device_id roseline_dt_ids[] = {
  { .compatible = "roseline", },
  { /* sentinel */ }
};

MODULE_DEVICE_TABLE(of, roseline_dt_ids);

// Initialise the kernel driver
static int roseline_probe(struct platform_device *pdev)
{
  const struct of_device_id *match;
  struct roseline_platform_data *pdata;
  struct pinctrl *pinctrl;

  // Try and find a device that matches "compatible: roseline"
  match = of_match_device(roseline_dt_ids, &pdev->dev);
  if (match)
    pdev->dev.platform_data = of_get_roseline_pdata(pdev);
  else
    pr_err("of_match_device failed\n");
  
  // Get the platform data
  pdata = pdev->dev.platform_data;
  if (!pdata)
    return -ENODEV;
  pdata->clk_ext.ready = 0;
  pdata->clk_int.ready = 0;
  pdata->clk_rtc.ready = 0;

  // Populate the /sys tree 
  if (sysfs_create_group(&pdev->dev.kobj, &attr_group))
    pr_err("sysfs_create_group failed\n");
 
  // Setup pin control
  pinctrl = devm_pinctrl_get_select_default(&pdev->dev);
  if (IS_ERR(pinctrl))
    pr_warning("pins are not configured from the driver\n");

  // Setup the clock sources for each
  omap_dm_timer_use_clk(&(pdata->clk_ext),OMAP_TIMER_SRC_EXT_CLK);
  omap_dm_timer_use_clk(&(pdata->clk_int),OMAP_TIMER_SRC_SYS_CLK);
  omap_dm_timer_use_clk(&(pdata->clk_rtc),OMAP_TIMER_SRC_32_KHZ);

  // Enable interrupts on the clocks
	roseline_enable_irq(&(pdata->clk_ext));
  roseline_enable_irq(&(pdata->clk_int));
  roseline_enable_irq(&(pdata->clk_rtc));
  
  // Set all clocks ready
  pdata->clk_ext.ready = 1;
  pdata->clk_int.ready = 1;
  pdata->clk_rtc.ready = 1;

  return 0;
}

// Remove the kernel driver
static int roseline_remove(struct platform_device *pdev)
{
  struct roseline_platform_data *pdata = pdev->dev.platform_data;
  if (pdata)
  {
    roseline_cleanup_timer(&(pdata->clk_ext));
    roseline_cleanup_timer(&(pdata->clk_int));
    roseline_cleanup_timer(&(pdata->clk_rtc));
    devm_kfree(&pdev->dev, pdata);
    pdev->dev.platform_data = NULL;
    sysfs_remove_group(&pdev->dev.kobj, &attr_group);
  }
  platform_set_drvdata(pdev, NULL);
  return 0;
}

static struct platform_driver roseline_driver = {
  .probe    = roseline_probe,				/* Called on module init */
  .remove   = roseline_remove,				/* Called on module kill */
  .driver   = {
    .name = MODULE_NAME,			
    .owner = THIS_MODULE,
    .of_match_table = of_match_ptr(roseline_dt_ids),
  },
};

module_platform_driver(roseline_driver);