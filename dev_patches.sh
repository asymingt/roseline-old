#!/bin/bash                               
FOLDER_OLD="linux-3.18.9-vanilla"
FOLDER_NEW="linux-3.18.9"
FOLDER_PATCHES="./patches"

echo "Generating changelog for $FOLDER_OLD --> $FOLDER_NEW"
diff -qdr ${FOLDER_OLD} ${FOLDER_NEW} | sed "s/^.* and \(.*\) differ/\1/" | sort > newchanges.txt

mkdir -p ${FOLDER_PATCHES}                                                                                                           
for newfile in `find $FOLDER_NEW -type f`
do
sufix=${newfile#${FOLDER_NEW}/}
oldfile="${FOLDER_OLD}/${sufix}"

if [ -f $oldfile ]; then
    patchfile="${FOLDER_PATCHES}/`echo $sufix |sed 's/\//_/g'`.patch"
    if [ "`diff $oldfile $newfile`" != "" ]; then
        echo "Make patch ${patchfile}"
        diff -dupN "$oldfile" "$newfile" > "$patchfile"
    fi
else
patchfile="${FOLDER_PATCHES}/ADDONS/${newfile#$FOLDER_NEW}"
    echo "Copy $newfile --> $patchfile"
    mkdir -p "`dirname ${patchfile}`"
    cp $newfile $patchfile
fi
done