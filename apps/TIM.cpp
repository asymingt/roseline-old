#include "TIM.h"

#include <iostream>
#include <fstream>
#include <cstdlib>

// Constructor
TIM::TIM() : last(0), overflow(0)
{}

// Destructor
TIM::~TIM()
{}

// Initialiser
bool TIM::init(const std::string &f)
{
	// Check that we can open and read the file
	file = f; uint64_t tmp;
	return (read(tmp) >= 0);
}

// Read the file value
bool TIM::read(uint64_t &cnt)
{
	// Grab the value
	std::ifstream t(file.c_str());
	if (!t.is_open())
		return false;

	// Read the whole file
	std::string str((std::istreambuf_iterator<char>(t)),
                     std::istreambuf_iterator<char>());
	t.close();

	// Convert string to unsigned long
	cnt = (uint64_t) strtoul(str.c_str(), NULL, 10);

	// Return the value
	return true;
}