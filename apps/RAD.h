#ifndef RAD_H
#define	RAD_H

#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

// Message types
#define TIMESYNC_REQ 0x1
#define TIMESYNC_RES 0x2

// Data structure epoch
typedef struct
{
    uint8_t  mid;	// Message ID
    uint16_t nid;	// Sender ID
    uint32_t seq;	// Sequence number
    uint64_t cnt;	// Count value
    uint64_t gps;	// GPS value
} 
msg_timesync;

// Radio request and response types
typedef void (*RxResType)(uint32_t &seq, uint16_t &id, uint64_t &cnt, uint64_t &gps);
typedef void (*RxReqType)(uint32_t &seq);

class RAD
{
public: RAD();
public: ~RAD();
public: bool init(const unsigned short &pan, const unsigned short &add, RxReqType rx_req, RxResType rx_res);
public: bool tx_res(uint32_t &seq, uint64_t &cnt, uint64_t &gps);
public: bool tx_req(uint32_t &seq);
private: void work();
private: int sd;			// Device descriptor
private: uint16_t addr;			// My address
private: boost::thread listen;		// Listen thread
private: RxReqType cb_req;		// Request callback
private: RxResType cb_res;		// Response callback
};

#endif
