#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <iostream>

#define PAGE_SIZE 4096

int main ( int argc, char **argv )
{
	int fd = open("/dev/mem", O_RDWR | O_SYNC);
	volatile uint32_t *t4r = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x48040000); 
	volatile uint32_t *t5r = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x48046000); 
	volatile uint32_t *t6r = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x48048000); 
	uint32_t t4 = t4r[0x3c/4]; 
	uint32_t t5 = t5r[0x3c/4]; 
	uint32_t t6 = t6r[0x3c/4]; 
	std::cout <<"T4: " << t4 << std::endl;
	std::cout <<"T5: " << t5 << std::endl;
	std::cout <<"T6: " << t6 << std::endl;
	return 0;
}
