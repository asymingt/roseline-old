#ifndef TIM_H
#define	TIM_H

#include <string>
#include <stdint.h>

class TIM
{
public: TIM();
public: ~TIM();
public: bool init(const std::string &f);
public: bool read(uint64_t &cnt);
private: std::string file; 
private: uint64_t last, overflow; 
};

#endif
