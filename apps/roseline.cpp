    // Basic printing
#include <iostream>
#include <fstream>

// UBLOX protocol decoder
#include "UBX.h"
#include "TIM.h"
#include "RAD.h"

// ASIO
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

/* There is no distinction between a root and node in this time synchronisation 
   code. All nodes collect epochs selfishly by randomly broadcasting time sync
   requests to their neighbours. When a request is received, the peers refrain
   from sending their pown epochs for a short period to allow collection.       */

// Data structure epoch
typedef struct
{
    uint64_t cnt;
    uint64_t gps;
}
Mark;

// Data structure epoch
typedef struct
{
	Mark 						loc;	// My time
    std::map<uint16_t,Mark> 	glo;	// Peer times [ID] -> Mark
} 
Epoch;

// Vector of epochs [SEQ] -> Mark
std::map<uint32_t,Epoch> data;

// Modular functionality
UBX ubx;    // GNSS module
TIM tim;    // Time capture
RAD rad;    // Radio module

// My sequence number
uint32_t myseq = 0;
uint32_t mynum = 0;

// For data writing
std::ofstream csv;

// Send out a request
void do_stats(const boost::system::error_code& /*e*/, boost::asio::deadline_timer *t,
    uint16_t &ms)
{
    // Immediately reschedule a timer
    if (myseq < mynum)
    {
    	t->expires_at(t->expires_at() + boost::posix_time::milliseconds(ms));
    	t->async_wait(boost::bind(do_stats, boost::asio::placeholders::error, t, ms));
    }

    uint32_t n = data.size();
    uint32_t m = 0;
    for (std::map<uint32_t,Epoch>::iterator i = data.begin(); i != data.end(); ++i)
    	m += i->second.glo.size();

    // Display results
    std::cout << "RESULTS" << std::endl;
    std::cout << "- Number of probes: " << n << std::endl;
    std::cout << "- Number of responses: " << m << std::endl;
}

// Send out a request
void do_probe(const boost::system::error_code& /*e*/, boost::asio::deadline_timer *t,
    uint16_t &ms)
{
    // Immediately reschedule a timer
    if (myseq < mynum)
    {
    	t->expires_at(t->expires_at() + boost::posix_time::milliseconds(ms));
    	t->async_wait(boost::bind(do_probe, boost::asio::placeholders::error, t, ms));
    }
	std::cout << "BEGIN PROBE" << std::endl;

    // For storing the local epoch
    Epoch epoch;

    // 1. BROADCAST A TIMESYNC REQ MESSAGE
    if (!rad.tx_req(myseq))
    {
        std::cout << "Packet transmission error" << std::endl;
        return;
    }
    std::cout << "- REQUEST SEQ " << myseq << std::endl;

    // 2. READ LOCAL TIME CAPTURE
    if (!tim.read(epoch.loc.cnt))
    {
        std::cout << "Time capture read error" << std::endl;
        return;
    }
    std::cout << "- CNT : "  << epoch.loc.cnt << std::endl;

    // 3. BLOCKING WAIT FOR UBLOX TIME MARK
    if (!ubx.read(epoch.loc.gps))
    {
        std::cout << "Ublox read error" << std::endl;
        return;
    }
    std::cout << "- GPS : "  << epoch.loc.gps << std::endl;

    // 5. DO SOMETHING WITH THE MEASUREMENTS
	data[myseq++] = epoch;
}

// Collect response to our request
void rx_res(uint32_t &seq, uint16_t &id, uint64_t &cnt, uint64_t &gps)
{
	std::cerr << "Received RESPONSE from " << id << " with SEQ " << seq <<std::endl;

	// Create a time mark
	Mark mark;
	mark.cnt = cnt;
	mark.gps = gps;

	// For storing the local epoch
	data[seq].glo[id] = mark;
}

// Respond to a request
void rx_req(uint32_t &seq)
{
	std::cerr << "Received REQUEST " << std::endl;

    // Temporary storage
	Mark tmp;

    // 1. READ LOCAL TIME CAPTURE
    if (!tim.read(tmp.cnt))
    {
        std::cout << "Time capture read error" << std::endl;
        return;
    }
    std::cout << "- CNT : "  << tmp.cnt << std::endl;

    // 2. WAIT FOR UBLOX TIME MARK
    if (!ubx.read(tmp.gps))
    {
        std::cout << "Ublox read error" << std::endl;
        return;
    }
    std::cout << "- GPS : "  << tmp.gps << std::endl;

    // 3. PACKAGE AND SEND
    if (!rad.tx_res(seq,tmp.cnt,tmp.gps))
    {
        std::cout << "Radio read error" << std::endl;
        return;
    }
    std::cout << "RX: RESPONSE SEQ " << seq << std::endl;
}

// Main entry point of application
int main(int argc, char **argv)
{
    // Parse command line options
    boost::program_options::options_description desc("Allowed options");
    desc.add_options()
      ("help,h",  "produce help message")
      ("cap,c",   boost::program_options::value<std::string>()->default_value("/sys/devices/roseline/ext_capture"), "Time capture file")
      ("dev,d",   boost::program_options::value<std::string>()->default_value("/dev/ttyO4"), "GNSS serial device")
      ("out,o",   boost::program_options::value<std::string>()->default_value("data.csv"), "File to save data")
      ("num,n",   boost::program_options::value<unsigned short>()->default_value(1000), "Number of probes")
      ("baud,b",  boost::program_options::value<unsigned short>()->default_value(9600), "GNSS serial baud")
      ("pan,p",   boost::program_options::value<unsigned short>()->default_value(0xFEED), "IEEE 802.15.4 PAN ID")
      ("addr,a",  boost::program_options::value<unsigned short>()->default_value(0x0001), "IEEE 802.15.4 ADDRESS")
      ("int,i",   boost::program_options::value<unsigned short>()->default_value(5000), "Average probe interval (ms)")
      ("tim,t",   boost::program_options::value<unsigned short>()->default_value(1000), "Average time sync interval")
    ;
    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);    

    // Print some help with arguments
    if (vm.count("help") > 0)
    {
        std::cout << desc << "\n";
        return 0;
    }

    // Try and connect to the UBLOX serial
    if (!ubx.init(vm["dev"].as<std::string>(), vm["baud"].as<unsigned short>()))
    {
        std::cerr << "Could not connect to GNSS serial" << std::endl;
        return 1;
    }

    // Try and connect to the time capture
    if (!tim.init(vm["cap"].as<std::string>()))
    {
        std::cerr << "Could not open the time capture file" << std::endl;
        return 1;
    }

    // Try and initialise the radio, with callbacks
    uint16_t addr =  vm["addr"].as<unsigned short>();
    if (!rad.init(vm["pan"].as<unsigned short>(), addr, &rx_req, &rx_res))
    {
        std::cerr << "Could not initialise the radio" << std::endl;
        return 1;
    }

    // Sequence number and total probes
	myseq = 0;
	mynum = vm["num"].as<unsigned short>();

	// Start periodic synchronisation
    boost::asio::io_service io;
    uint16_t tp = vm["int"].as<unsigned short>();
	boost::asio::deadline_timer t_probe(io, boost::posix_time::milliseconds(tp));
	if (tp > 0)
    	t_probe.async_wait(boost::bind(do_probe, boost::asio::placeholders::error, &t_probe, tp));	
	uint16_t tt = vm["tim"].as<unsigned short>();
	boost::asio::deadline_timer t_stats(io, boost::posix_time::milliseconds(tt));
	if (tt > 0)
		t_stats.async_wait(boost::bind(do_stats, boost::asio::placeholders::error, &t_stats, tt));
    io.run();

    // Try and open
  	csv.open(vm["out"].as<std::string>().c_str());
  	if (!csv.is_open())
    {
        std::cerr << "Could not open data output file" << std::endl;
        return 1;
    }

    // Write CSV
    for (std::map<uint32_t,Epoch>::iterator i = data.begin(); i != data.end(); ++i)
    	for (std::map<uint16_t,Mark>::iterator j = i->second.glo.begin(); j != i->second.glo.end(); ++j)
			csv << addr << "," 
				<< j->first << "," 
				<< i->second.loc.cnt << "," 
				<< i->second.loc.gps << ","
				<< j->second.cnt << ","
				<< j->second.gps << std::endl;
    
    // Write data to file
    csv.close();

    // Everything OK
    return 0;
}
