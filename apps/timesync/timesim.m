m = 20;     % Number of nodes
n = 300;    % Number of time steps
R = 0.4;    % Communication distance (% of side length)
f = 8e6;    % Fundamental clock frequency
t = 5;      % Sample rate
s = 10e5;   % Interval from which offset is uniformly drawn
p = 0.001;   % ppm error for oscillators
h = 11.65;  % time capture error

% Randomize starting positions
x = rand(m, 2);

% Calculate a distance matrix
D = squareform(pdist(x));

% Remove all edges greater than r
D(D > R) = 0;

% Create a connectivity matrix
E = triu(D > 0);

% Get the communciation pairs
[r,c] = find(E);

% Draw the communication graph
figure; grid on; hold on;
a = plot(x(:,1),x(:,2),'ko');
for i = 1:length(r)
    b = plot(x([r(i) c(i)],1),x([r(i) c(i)],2),'k');
end

% By default, (1) is the root node
c_r(1,:) = [1:n] * t * f;
z_r(1,:) = floor(c_r(1,:));

%figure; grid on; hold on;

% Now, simulate time sync
for i = 2:length(r)
   
    % Select a random offset
    k = s * rand(); 
    
    % Random frequency perturbations
    f_c = sqrt(p * 8) * randn();
    f_w = sqrt(p * f / 1e6) * randn(1,n);
    
    % Count at the node
    c_n(i,:) = k + cumsum(t .* (f + f_c)) + sqrt(h) * randn(1,n);
    z_n(i,:) = floor(c_n(i,:));

end

% Ornstein-Uhlenbeck
c = zeros(1,M);
for j = 1:M
    s_o(1) = x;
    for i = 2:N
        k = exp(-d/r);
        s_o(i) = k*s_o(i-1) + (1-k)*m + sqrt(v*(1-k*k)*r/2) * randn();
    end
    c(j) = s_o(end);
end
k = exp(-T/r);


