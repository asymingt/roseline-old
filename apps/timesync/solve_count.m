function F = solve_count(x,clkr,clkn,tcv,ppm)

    % Number of time epochs
    n = length(clkr);

    % Extract latent variables
    f_n = x(1:n);            % Count error at node
    k   = x(n+1);            % Offset
    
    % Extract frequency errors and time from count prediction
    dt = (clkr(2:end) - clkr(1:end-1)) / 8e6;
    fa = (f_n(2:end) + f_n(1:end-1)) / 2;
    
    % Model absolute time (sampled with quantization variance)
    F0 = (k + [0, cumsum(dt ./ fa)]) - (clkr - clkn);
    F0 = F0 ./ tcv;
    
    % Intial offset must be within 1 time capture error of observation
    F1 = k - (clkr(1) - clkn(1));
    F1 = F1 ./ tcv;
   
    % Model drift as a random process (v_s in 1s, therefore dt*v_s in dt)
    F2 = f_n(2:end) - f_n(1:end-1);
    F2 = F2 ./ (ppm * 8 * dt);
    
    % Constrain the first value
    F = [F0, F1, F2];
    %max(F0)
    %max(F1)
    %max(F2)
    %ouch
    
end