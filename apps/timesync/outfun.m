function stop = outfun(x,optimValues,state)
    stop = false;
    
    % Extract important variables
    n = length(x)-1;
    
    % Extract latent variables
    f_n = x(0*n+1:1*n);      % Count error at node
    k   = x(n+1);            % Offset
    
    % Extract frequency errors and time from count prediction
    c_n = cumsum(5./f_n);
   
    % Plot everything
    subplot(1,3,1); grid on;
    plot(k*ones(1,n),'b');

    subplot(1,3,2); grid on;
    plot(c_n,'b');

    subplot(1,3,3); grid on;
    plot(f_n,'b');

    drawnow;
end 