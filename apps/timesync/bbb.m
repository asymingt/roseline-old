% Grab the data
a = importdata('data1.csv');
a = a(10:end,:);

% Extract CNT and GPS
c_a = overflow(a(:,3)');
g_a = a(:,4)';
c_b = overflow(a(:,5)');
g_b = a(:,6)';

err = c_a - c_b;
err = err - err(1);

% Remove 9us to get actual trigger 
figure; grid on; hold on;
plot(g_a - g_b - mean(g_a - g_b));
xlabel('Sample');
ylabel('Count error (ns)');
title('Random walk of GNSS time marking error');

%i = find(abs(g) < 15000 & g ~= 0);
%a = a(i,:);
%a = a(10:end,:);
%plot(a(:,4) - a(:,6));

%c_a = overflow(a(:,3)');
%c_b = overflow(a(:,5)');

% There should be no drift, so all we see is capture error
%err = c_a - c_b;
%err = [0, err(2:end) - err(1:(end-1))]; % Relative error
%tcv = var(err);                         % Capture
