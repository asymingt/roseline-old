function F = solve_skew(x,err,tim)

    % x is now 2n [f0 ... fN, df0 ... dfN]

    % Number of time epochs
    n = length(err);

    % Try and keep the frequency error the same (penalize big changes)
    F(1:n-1) = x(2:n) - x(1:n-1);
    
    % Make T / f = clock error
    F(n:n+n-2) = tim(2:n) ./ x(2:n) - err(2:n);
        
end