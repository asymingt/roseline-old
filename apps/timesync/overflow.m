function x = overflow(x)
    of = 0;
    for i = 2:length(x)
        x(i) = x(i) + (2^32 - 1) * of;
        if x(i) < x(i-1)
           of = of + 1; 
           x(i) = x(i) + (2^32 - 1);
        end
    end
end