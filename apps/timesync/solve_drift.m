function F = solve_drift(x,err,tim,v_t,v_s,v_n)

    % x is now 2n [f0 ... fN, df0 ... dfN]
    
    % Number of time epochs
    n = length(err);

    % Try keep constant drift
    F(1:n-1) = (x(n+2:n+n) - x(n+1:n+n-1)) / v_s;
    
    % Make sure that the frequency evolution matches drift
    % Intuition: e = f1 - f0 + dT * mean(dfT,dfT-1)
    F(n:n+n-2) = ((x(1:n-1) + tim(2:n) .* ((x(n+2:n+n)+x(n+1:n+n-1))/2)) - x(2:n)) / v_n;
    
    % Ensure the frequency error matches the clock error
    % Intuition: e = c - dT / mean(fT,fT-1)
    F(n+n-1:n+n+n-3) = (tim(2:n) ./ ((x(2:n) + x(1:n-1)) / 2) - err(2:n))  / v_t;
    
end