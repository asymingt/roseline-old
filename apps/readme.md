This is a straightforward BeagleBone Black application which works above the Linux 3.18.9-rt kernel
and roseline plugin. You require an at86rf233 module to be attached to SPI0, with the DIG2 interrupt
line fed into the timer4, timer5, timer6 lines on the BeagleBone Black. You should also connect this
line to INT0 on a LEA-M8F toming module, which is connected to the BeagleBone Black on UART4.

The purpose of this application is to provide the underlying data that allows for time synchronisation
between nodes on a network. Consider an epoch to be a single point in a global time frame. Neglecting
propagation delay (3ns/m), the at86rf233 has a 9us + N(0,200ns) delay between the DIG2 interrupt on
the RX and TX side respectively. Using this fact we can trigger a time mark at both the receiver and 
transmitter. They can exchange this information at regular intervals to align time.

The algorithm is based on a flooding protocol, driven by periodic random pulses. Consider the network
to be comprised of N nodes. This application takes the following parameters:
	
	- l : the exponentially distributed pulses
	- t : timeout waiting for request

This happens at each node:

	THREAD1						

	- BEGIN :
	- WAIT(D ~ EXPDIST(l))
	- IF LOCK(L) GOTO BEGIN
	- LOCK(L)
	- TRANSMIT TS_REQ
	- OBSERVE BBB TIME CAPTURE REGISTER
	- OBSERVE GPS TIME CAPTURE REGISTER
	- COLLECT TS_RES, OR TIMEOUT
	- LOG DATA
	- UNLOCK(L)
	- GOTO BEGIN

	THREAD2

	- BEGIN :
	- WAIT UNTIL TS_REQ
	- LOCK(L)
	- OBSERVE BBB TIME CAPTURE REGISTER
	- OBSERVE GPS TIME CAPTURE REGISTER
	- TRANSMIT TS_RES
	- UNLOCK(L)
	- GOTO BEGIN
