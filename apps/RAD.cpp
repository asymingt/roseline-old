#include "RAD.h"

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Needed for sockets
#include "ieee802154.h"

// Constructor
RAD::RAD() {}

// Destructor
RAD::~RAD() {}

// Initialiser
void RAD::work()
{
	while (1)
	{
		msg_timesync buf;
		fd_set rs, ws, xs;
		int r, w, ret;

		FD_ZERO(&rs);
		FD_ZERO(&ws);
		FD_ZERO(&xs);
		FD_SET(sd, &rs);
		FD_SET(sd, &xs);
		FD_SET(0, &rs);

		// Wait for incoming packets
		ret = select(sd + 1, &rs, &ws, &xs, NULL);
		if (FD_ISSET(sd, &rs))
		{
			ret = read(sd, &buf, sizeof(buf));
			if (ret > 0) 
			{
				// Notify the master thread that something has happened
		 		switch (buf.mid)
		 		{
		 			case TIMESYNC_REQ: 
		 				cb_req(buf.seq); 
		 				break;
		 			case TIMESYNC_RES: 
		 				cb_res(buf.seq, buf.nid, buf.cnt, buf.gps); 
		 				break;
		 		}
		 	}
		 }
	}
}

// Initialiser
bool RAD::init(const unsigned short &pan, const unsigned short &add, RxReqType rx_req, RxResType rx_res)
{
	// Save the callbacks
	cb_req = rx_req;
	cb_res = rx_res;

	// Return status and socket
	struct sockaddr_ieee802154 a;

	// Open an IEEE 802.15.4 socket
	sd = socket(PF_IEEE802154, SOCK_DGRAM, 0);
	if (sd < 0)
	{
		std::cerr << "Could not open the socket" << std::endl;
		return false;
	}

	// Set the PAN and address
	a.family = AF_IEEE802154;
	a.addr.addr_type = IEEE802154_ADDR_SHORT;
	a.addr.pan_id = pan;
	a.addr.short_addr = add;
	if (bind(sd, (struct sockaddr *)&a, sizeof(a)))
	{
		std::cerr << "Could not bind to the socket" << std::endl;
		return false;
	}

	// Set broadcast address (all neighbours should respond)
	a.addr.short_addr = 0xFFFF;
	if (connect(sd,(struct sockaddr *)&a, sizeof(a)))
	{
		std::cerr << "Could not connect to the socket" << std::endl;
		return false;
	}

	// Thread to continually listen for packets
	listen = boost::thread(boost::bind(&RAD::work,this));
    
	// Backupt he address
	addr = add;

	// Success!
	return true;
}

// Transmit a response
bool RAD::tx_res(uint32_t &seq, uint64_t &cnt, uint64_t &gps)
{
	// Set the important variables
	msg_timesync rx;
	rx.mid = TIMESYNC_RES;
	rx.nid = addr;
	rx.seq = seq;
	rx.cnt = cnt;
	rx.gps = gps;
	return (write(sd, &rx, sizeof(rx)) == sizeof(rx));
}

// Transmit a reqyest
bool RAD::tx_req(uint32_t &seq)
{
	// Set the important variables
	msg_timesync tx;
	tx.mid = TIMESYNC_REQ;
	tx.nid = addr;
	tx.seq = seq;
	return (write(sd, &tx, sizeof(tx)) == sizeof(tx));
}