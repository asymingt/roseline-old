#! /bin/sh
# mkcard.sh v0.5
# (c) Copyright 2009 Graeme Gregory <dp@xora.org.uk>
# Licensed under terms of GPLv2
#
# Parts of the procudure base on the work of Denys Dmytriyenko
# http://wiki.omap.com/index.php/MMC_Boot_Format

if [ $# -ne 1 ]; then
	echo "Usage: $0 <drive>"
	exit 1;
fi

echo "> Assuming particular drive structure ..."
DRIVE=$1
PARTITION1=${DRIVE}1
PARTITION2=${DRIVE}2

# Copy over the BOOT files
echo "> Preparing boot parition..."
mkdir -p tmp/boot
mount ${PARTITION1} tmp/boot
cp bootloader/* tmp/boot
cp linux/arch/arm/boot/uImage tmp/boot
cp linux/arch/arm/boot/dts/am335x-boneblack.dtb tmp/boot
umount tmp/boot

# Copy over the ROOTFS files
echo "> Preparing rootfs parition..."
mkdir -p tmp/rootfs
mount ${PARTITION2} tmp
tar -xpJf rootfs/rootfs.tar.xz -C tmp/rootfs
ROOTFS=$PWD/tmp/rootfs
pushd linux
	make ARCH=arm INSTALL_MOD_PATH=$ROOTFS modules_install
popd
pushd module
	cp roseline.ko $ROOTFS/lib/modules/3.18.9-rt4/kernel/drivers
popd
umount tmp